(function($) {

  var $element;

  /**
   * Check to find a matching element based on module settings. If the element
   * exists then fetch message via ajax request.
   */
  Drupal.behaviors.ajaxMessages = {
    attach: function (context, settings) {
      var ajax_msgs = this;

      // Don't re-run this function when we trigger behaviours after adding messages.
      if(Drupal.settings.ajax_msgs.fetchingMessages === true) {
        Drupal.settings.ajax_msgs.fetchingMessages = false;
        return;
      }

      // Set the element used to add messages to.
      $.each(Drupal.settings.ajax_msgs.elements, function(index, element) {
        if($('#' + element).length) {
          $element = $('#' + element);
        }
      });

      if(typeof $element == 'undefined' || !$element.length) {

        // Set an error if the element does not exist.
        var settings_page = window.location.protocol + '://' + window.location.hostname +  settings.basePath + settings.pathPrefix + '/admin/config/user-interface/ajax-messages';
        var err = 'Ajax messages can\'t put any messages on your page as there is no matching element. Check your settings at ' + settings_page;
        if (window.console) {
          console.error(err);
        } else {
          alert(err);
        }

      } else {

        // Trigger an ajax call to fetch the messages.
        $element.once('ajax-messages', function() {
          // ******************************************************************
          // This was causing problems with offline, so I switched the AJAX method from Drupal.ajax to plain jQuery
          // ******************************************************************
          //
          // var base = $(this).attr('id');
          // var element_settings = {
          //   url: 'http://' + window.location.hostname +  settings.basePath + settings.pathPrefix + 'ajax-messages/fetch',
          //   event: 'fetch',
          //   progress: {
          //     type: 'throbber'
          //   }
          // };
          // Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
          // $(this).trigger('fetch');
          // Drupal.settings.ajax_msgs.fetchingMessages = true;
          $.ajax({
            url: settings.basePath + settings.pathPrefix + 'ajax-messages/fetch',
            method: 'POST',
            contentType: 'application/json; charset=UTF-8',
            success: function(data, status, xhr) {
              var response = false;
              data.forEach(function(item) {
                if(item.command == 'themeAjaxMessages') {
                  response = item;
                }
              });

              if(response) {
                ajax_msgs.themeAjaxMessages(response, $element);
              }
            },
            error: function(xhr, status, error) {
            }
          });
        });

      }

    },

    themeAjaxMessages: function(response, $element) {
      var html = '';
      $.each(response.data.messages, function(type, messages) {
        html += '<div class="messages ' + type + '">';
        if (response.data.headings[type].length) {
          html += '<h2 class="element-invisible">' + response.data.headings[type] + '</h2>';
        }
        if (messages.length) {
          html += '<ul>';
          $.each(messages, function(index, message) {
            html += '<li>' + message + '</li>';
          });
          html += '</ul>';
        }
        html += '</div>';
      });

      $element[Drupal.settings.ajax_msgs.method](html);

      // Attach behaviours to newly added messages
      Drupal.attachBehaviors('#messages');
    },

    // ## detach()
    //
    // To be consistent with Drupal standards, we provide an unattach function
    detach: function (context, settings, trigger) {

    }
  };
  

  /**
   * Theme messages ajax callback.
   */
  // ******************************************************************
  // Now that we are using jQuery instead of Drupal.ajax, this function was moved to ajax_msgs
  // ******************************************************************
  // Drupal.ajax.prototype.commands.themeAjaxMessages = function(ajax, response, status) {

  //   var html = '';
  //   $.each(response.data.messages, function(type, messages) {
  //     html += '<div class="messages ' + type + '">';
  //     if (response.data.headings[type].length) {
  //       html += '<h2 class="element-invisible">' + response.data.headings[type] + '</h2>';
  //     }
  //     if (messages.length) {
  //       html += '<ul>';
  //       $.each(messages, function(index, message) {
  //         html += '<li>' + message + '</li>';
  //       });
  //       html += '</ul>';
  //     }
  //     html += '</div>';
  //   });

  //   $element[Drupal.settings.ajax_msgs.method](html);

  //   // Attach behaviours to newly added messages
  //   Drupal.attachBehaviors('#messages');

  // };

})(jQuery);
