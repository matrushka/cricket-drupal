<?php

/**
 * @file
 * Puts messages on the page using a ajax call.
 */

/**
 * Implements hook_menu().
 */
function ajax_msgs_menu() {
  $items = array();
  $items['admin/config/user-interface/ajax-messages'] = array(
    'title' => 'Ajax messages',
    'type' => MENU_NORMAL_ITEM,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ajax_msgs_admin_form'),
    'file' => 'ajax_msgs.admin.inc',
    'access arguments' => array('administer ajax messages'),
    'description' => 'Set where the ajax messages are added to the page',
  );
  $items['ajax-messages/fetch'] = array(
    'title' => 'Fetch ajax messages',
    'type' => MENU_CALLBACK,
    'page callback' => 'ajax_msgs_fetch_messages',
    'delivery callback' => 'ajax_deliver',
    'access arguments' => array('access content'),
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function ajax_msgs_permission() {
  return array(
    'administer ajax messages' => array(
      'title' => t('Administer ajax messages'),
      'description' => t('Allows the user to choose where the ajax messages are added to the page.'),
    ),
  );
}

/**
 * Implements hook_theme_registry_alter().
 */
function ajax_msgs_theme_registry_alter(&$theme_registry) {
  $theme_registry['status_messages']['function'] = 'ajax_msgs_status_messages';
}

/**
 * Callback function to theme status messages during page build.
 */
function ajax_msgs_status_messages() {
  // Nothing to do here.
}

/**
 * Implements hook_init().
 */
function ajax_msgs_init() {
  $settings = array(
    'fetching' => FALSE,
    'elements' => array_map('trim', explode(',', variable_get('ajax_msgs_element', 'messages'))),
    'method' => variable_get('ajax_msgs_element_method', 'inside'),
  );
  drupal_alter('ajax_msgs_settings', $settings);
  drupal_add_js(array('ajax_msgs' => $settings), 'setting');
  drupal_add_library('system', 'drupal.ajax');
  drupal_add_js(drupal_get_path('module', 'ajax_msgs') . '/ajax-messages.js');
}

/**
 * Menu callback function to fetch all messages.
 */
function ajax_msgs_fetch_messages() {

  $data = array(
    'messages' => drupal_get_messages(),
    'headings' => array(
      'status' => t('Status message'),
      'error' => t('Error message'),
      'warning' => t('Warning message'),
    ),
  );

  drupal_alter('ajax_msgs', $data);

  $commands[] = array(
    'command' => 'themeAjaxMessages',
    'data' => $data,
  );

  return array('#type' => 'ajax', '#commands' => $commands);

}
