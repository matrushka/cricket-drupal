<?php
/**
 * @file
 * cricket_content_types.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function cricket_content_types_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cr_sharing|node|cr_note|form';
  $field_group->group_name = 'group_cr_sharing';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cr_note';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Sharing',
    'weight' => '5',
    'children' => array(
      0 => 'field_cr_sharing_everyone',
      1 => 'field_cr_sharing_groups',
      2 => 'field_cr_sharing_users',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-cr-sharing field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_cr_sharing|node|cr_note|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Sharing');

  return $field_groups;
}
