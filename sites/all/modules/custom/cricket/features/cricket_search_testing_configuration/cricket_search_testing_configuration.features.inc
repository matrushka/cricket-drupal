<?php
/**
 * @file
 * cricket_search_testing_configuration.features.inc
 */

/**
 * Implements hook_default_search_api_index().
 */
function cricket_search_testing_configuration_default_search_api_index() {
  $items = array();
  $items['notes_2'] = entity_import('search_api_index', '{
    "name" : "Notes",
    "machine_name" : "notes_2",
    "description" : null,
    "server" : "cricket_testing_os_x",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [ "cr_note" ] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "author" : { "type" : "integer", "entity_type" : "user" },
        "field_cr_attendees" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_cr_body:value" : { "type" : "text" },
        "field_cr_date" : { "type" : "date" },
        "field_cr_internal_attendees" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "user" },
        "field_cr_organizations" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_cr_pinned" : { "type" : "boolean" },
        "field_cr_sharing_everyone" : { "type" : "boolean" },
        "field_cr_sharing_groups" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_cr_sharing_users" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "user" },
        "field_cr_topics" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_tags" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "nid" : { "type" : "integer" },
        "search_api_access_node" : { "type" : "list\\u003Cstring\\u003E" },
        "search_api_aggregation_1" : { "type" : "text" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "boolean" },
        "title" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-50",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "-49", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "-48", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "-47", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "-46", "settings" : [] },
        "search_api_alter_add_aggregation" : {
          "status" : 1,
          "weight" : "-45",
          "settings" : { "fields" : { "search_api_aggregation_1" : {
                "name" : "Full note text",
                "type" : "fulltext",
                "fields" : [
                  "title",
                  "field_cr_attendees",
                  "field_cr_internal_attendees",
                  "field_cr_organizations",
                  "field_tags",
                  "field_cr_body:value"
                ],
                "description" : "A Fulltext aggregation of the following fields: Title, Attendees, Internal Attendees, Organizations, Tags, Body \\u00bb Text."
              }
            }
          }
        },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "-44", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "field_cr_body:value" : true } }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "search_api_aggregation_1" : true, "field_cr_body:value" : true },
            "title" : 1,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "field_cr_body:value" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "field_cr_body:value" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function cricket_search_testing_configuration_default_search_api_server() {
  $items = array();
  $items['cricket_testing_os_x'] = entity_import('search_api_server', '{
    "name" : "Cricket testing OS X",
    "machine_name" : "cricket_testing_os_x",
    "description" : "Solr server for testing purposes only.",
    "class" : "search_api_solr_service",
    "options" : {
      "clean_ids" : true,
      "site_hash" : true,
      "scheme" : "http",
      "host" : "192.168.99.100",
      "port" : "8983",
      "path" : "\\/solr\\/Cricket_test",
      "http_user" : "",
      "http_pass" : "",
      "excerpt" : 0,
      "retrieve_data" : 0,
      "highlight_data" : 0,
      "skip_schema_check" : 0,
      "solr_version" : "",
      "http_method" : "AUTO",
      "log_query" : 0,
      "log_response" : 0
    },
    "enabled" : "1"
  }');
  return $items;
}
