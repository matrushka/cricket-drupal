<?php
/**
 * @file
 * cricket_search_view.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cricket_search_view_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'cricket_search';
  $view->description = 'Basic Cricket note timelines.';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_notes_2';
  $view->human_name = 'Cricket Search';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Search';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'use cricket';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['search_api_bypass_access'] = 0;
  $handler->display->display_options['query']['options']['entity_access'] = 0;
  $handler->display->display_options['query']['options']['parse_mode'] = 'direct';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = TRUE;
  $handler->display->display_options['pager']['options']['expose']['offset'] = TRUE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  /* Field: Indexed Node: Node ID */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['link_to_entity'] = 0;
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 0;
  /* Field: Indexed Node: Body */
  $handler->display->display_options['fields']['field_cr_body']['id'] = 'field_cr_body';
  $handler->display->display_options['fields']['field_cr_body']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['field_cr_body']['field'] = 'field_cr_body';
  $handler->display->display_options['fields']['field_cr_body']['label'] = '';
  $handler->display->display_options['fields']['field_cr_body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cr_body']['settings'] = array(
    'data_element_key' => '',
    'skip_safe' => 0,
    'skip_empty_values' => 0,
    'skip_text_format' => 1,
  );
  /* Field: Indexed Node: Meeting Date */
  $handler->display->display_options['fields']['field_cr_date']['id'] = 'field_cr_date';
  $handler->display->display_options['fields']['field_cr_date']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['field_cr_date']['field'] = 'field_cr_date';
  $handler->display->display_options['fields']['field_cr_date']['label'] = '';
  $handler->display->display_options['fields']['field_cr_date']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_cr_date']['alter']['text'] = '[field_cr_date-value]';
  $handler->display->display_options['fields']['field_cr_date']['element_type'] = '0';
  $handler->display->display_options['fields']['field_cr_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cr_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => '',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Indexed Node: Topics */
  $handler->display->display_options['fields']['field_cr_topics']['id'] = 'field_cr_topics';
  $handler->display->display_options['fields']['field_cr_topics']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['field_cr_topics']['field'] = 'field_cr_topics';
  $handler->display->display_options['fields']['field_cr_topics']['label'] = '';
  $handler->display->display_options['fields']['field_cr_topics']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cr_topics']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_cr_topics']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_cr_topics']['bypass_access'] = 0;
  /* Field: Indexed Node: Organizations */
  $handler->display->display_options['fields']['field_cr_organizations']['id'] = 'field_cr_organizations';
  $handler->display->display_options['fields']['field_cr_organizations']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['field_cr_organizations']['field'] = 'field_cr_organizations';
  $handler->display->display_options['fields']['field_cr_organizations']['label'] = '';
  $handler->display->display_options['fields']['field_cr_organizations']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cr_organizations']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_cr_organizations']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_cr_organizations']['bypass_access'] = 0;
  /* Field: Indexed Node: Attendees */
  $handler->display->display_options['fields']['field_cr_attendees']['id'] = 'field_cr_attendees';
  $handler->display->display_options['fields']['field_cr_attendees']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['field_cr_attendees']['field'] = 'field_cr_attendees';
  $handler->display->display_options['fields']['field_cr_attendees']['label'] = '';
  $handler->display->display_options['fields']['field_cr_attendees']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cr_attendees']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_cr_attendees']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_cr_attendees']['bypass_access'] = 0;
  /* Field: Indexed Node: Internal Attendees */
  $handler->display->display_options['fields']['field_cr_internal_attendees']['id'] = 'field_cr_internal_attendees';
  $handler->display->display_options['fields']['field_cr_internal_attendees']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['field_cr_internal_attendees']['field'] = 'field_cr_internal_attendees';
  $handler->display->display_options['fields']['field_cr_internal_attendees']['label'] = '';
  $handler->display->display_options['fields']['field_cr_internal_attendees']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cr_internal_attendees']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_cr_internal_attendees']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_cr_internal_attendees']['bypass_access'] = 0;
  /* Field: Indexed Node: Author */
  $handler->display->display_options['fields']['author']['id'] = 'author';
  $handler->display->display_options['fields']['author']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['author']['field'] = 'author';
  $handler->display->display_options['fields']['author']['label'] = '';
  $handler->display->display_options['fields']['author']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['author']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['author']['display'] = 'id';
  $handler->display->display_options['fields']['author']['view_mode'] = 'full';
  $handler->display->display_options['fields']['author']['bypass_access'] = 0;
  /* Field: Indexed Node: Pin this note */
  $handler->display->display_options['fields']['field_cr_pinned']['id'] = 'field_cr_pinned';
  $handler->display->display_options['fields']['field_cr_pinned']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['field_cr_pinned']['field'] = 'field_cr_pinned';
  $handler->display->display_options['fields']['field_cr_pinned']['label'] = '';
  $handler->display->display_options['fields']['field_cr_pinned']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_cr_pinned']['alter']['text'] = '[field_cr_pinned-value]';
  $handler->display->display_options['fields']['field_cr_pinned']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cr_pinned']['settings'] = array(
    'data_element_key' => '',
    'skip_safe' => 0,
    'skip_empty_values' => 0,
  );
  /* Sort criterion: Indexed Node: Meeting Date */
  $handler->display->display_options['sorts']['field_cr_date']['id'] = 'field_cr_date';
  $handler->display->display_options['sorts']['field_cr_date']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['sorts']['field_cr_date']['field'] = 'field_cr_date';
  $handler->display->display_options['sorts']['field_cr_date']['order'] = 'DESC';
  $handler->display->display_options['sorts']['field_cr_date']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['field_cr_date']['expose']['label'] = 'Meeting Date';
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['operator'] = 'OR';
  $handler->display->display_options['filters']['search_api_views_fulltext']['group'] = 1;
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['label'] = 'Fulltext search';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['search_api_views_fulltext']['fields'] = array(
    'search_api_aggregation_1' => 'search_api_aggregation_1',
  );
  /* Filter criterion: Indexed Node: Topics */
  $handler->display->display_options['filters']['field_cr_topics']['id'] = 'field_cr_topics';
  $handler->display->display_options['filters']['field_cr_topics']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['filters']['field_cr_topics']['field'] = 'field_cr_topics';
  $handler->display->display_options['filters']['field_cr_topics']['group'] = 1;
  $handler->display->display_options['filters']['field_cr_topics']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_cr_topics']['expose']['operator_id'] = 'field_cr_topics_op';
  $handler->display->display_options['filters']['field_cr_topics']['expose']['label'] = 'Topics';
  $handler->display->display_options['filters']['field_cr_topics']['expose']['operator'] = 'field_cr_topics_op';
  $handler->display->display_options['filters']['field_cr_topics']['expose']['identifier'] = 'topics';
  $handler->display->display_options['filters']['field_cr_topics']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    4 => 0,
  );
  /* Filter criterion: Indexed Node: Organizations */
  $handler->display->display_options['filters']['field_cr_organizations']['id'] = 'field_cr_organizations';
  $handler->display->display_options['filters']['field_cr_organizations']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['filters']['field_cr_organizations']['field'] = 'field_cr_organizations';
  $handler->display->display_options['filters']['field_cr_organizations']['group'] = 1;
  $handler->display->display_options['filters']['field_cr_organizations']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_cr_organizations']['expose']['operator_id'] = 'field_cr_organizations_op';
  $handler->display->display_options['filters']['field_cr_organizations']['expose']['label'] = 'Organizations';
  $handler->display->display_options['filters']['field_cr_organizations']['expose']['operator'] = 'field_cr_organizations_op';
  $handler->display->display_options['filters']['field_cr_organizations']['expose']['identifier'] = 'organizations';
  $handler->display->display_options['filters']['field_cr_organizations']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    4 => 0,
  );
  /* Filter criterion: Indexed Node: Attendees */
  $handler->display->display_options['filters']['field_cr_attendees']['id'] = 'field_cr_attendees';
  $handler->display->display_options['filters']['field_cr_attendees']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['filters']['field_cr_attendees']['field'] = 'field_cr_attendees';
  $handler->display->display_options['filters']['field_cr_attendees']['group'] = 1;
  $handler->display->display_options['filters']['field_cr_attendees']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_cr_attendees']['expose']['operator_id'] = 'field_cr_attendees_op';
  $handler->display->display_options['filters']['field_cr_attendees']['expose']['label'] = 'Attendees';
  $handler->display->display_options['filters']['field_cr_attendees']['expose']['operator'] = 'field_cr_attendees_op';
  $handler->display->display_options['filters']['field_cr_attendees']['expose']['identifier'] = 'attendees';
  $handler->display->display_options['filters']['field_cr_attendees']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    4 => 0,
  );
  /* Filter criterion: Indexed Node: Internal Attendees */
  $handler->display->display_options['filters']['field_cr_internal_attendees']['id'] = 'field_cr_internal_attendees';
  $handler->display->display_options['filters']['field_cr_internal_attendees']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['filters']['field_cr_internal_attendees']['field'] = 'field_cr_internal_attendees';
  $handler->display->display_options['filters']['field_cr_internal_attendees']['group'] = 1;
  $handler->display->display_options['filters']['field_cr_internal_attendees']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_cr_internal_attendees']['expose']['operator_id'] = 'field_cr_internal_attendees_op';
  $handler->display->display_options['filters']['field_cr_internal_attendees']['expose']['label'] = 'Internal Attendees';
  $handler->display->display_options['filters']['field_cr_internal_attendees']['expose']['operator'] = 'field_cr_internal_attendees_op';
  $handler->display->display_options['filters']['field_cr_internal_attendees']['expose']['identifier'] = 'internal_attendees';
  $handler->display->display_options['filters']['field_cr_internal_attendees']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    4 => 0,
  );
  /* Filter criterion: Indexed Node: Meeting Date */
  $handler->display->display_options['filters']['field_cr_date']['id'] = 'field_cr_date';
  $handler->display->display_options['filters']['field_cr_date']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['filters']['field_cr_date']['field'] = 'field_cr_date';
  $handler->display->display_options['filters']['field_cr_date']['operator'] = '>=';
  $handler->display->display_options['filters']['field_cr_date']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_cr_date']['expose']['operator_id'] = 'field_cr_date_op';
  $handler->display->display_options['filters']['field_cr_date']['expose']['label'] = 'From';
  $handler->display->display_options['filters']['field_cr_date']['expose']['operator'] = 'field_cr_date_op';
  $handler->display->display_options['filters']['field_cr_date']['expose']['identifier'] = 'date_from';
  $handler->display->display_options['filters']['field_cr_date']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_cr_date']['group_info']['label'] = 'Meeting Date';
  $handler->display->display_options['filters']['field_cr_date']['group_info']['identifier'] = 'field_cr_date';
  $handler->display->display_options['filters']['field_cr_date']['group_info']['remember'] = FALSE;
  $handler->display->display_options['filters']['field_cr_date']['group_info']['group_items'] = array(
    1 => array(),
    2 => array(),
    3 => array(),
  );
  $handler->display->display_options['filters']['field_cr_date']['widget_type'] = 'date_popup';
  $handler->display->display_options['filters']['field_cr_date']['date_popup_format'] = 'Y-m-d';
  $handler->display->display_options['filters']['field_cr_date']['year_range'] = '-10:+0';
  /* Filter criterion: Indexed Node: Meeting Date */
  $handler->display->display_options['filters']['field_cr_date_1']['id'] = 'field_cr_date_1';
  $handler->display->display_options['filters']['field_cr_date_1']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['filters']['field_cr_date_1']['field'] = 'field_cr_date';
  $handler->display->display_options['filters']['field_cr_date_1']['operator'] = '<=';
  $handler->display->display_options['filters']['field_cr_date_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_cr_date_1']['expose']['operator_id'] = 'field_cr_date_1_op';
  $handler->display->display_options['filters']['field_cr_date_1']['expose']['label'] = 'Until';
  $handler->display->display_options['filters']['field_cr_date_1']['expose']['operator'] = 'field_cr_date_1_op';
  $handler->display->display_options['filters']['field_cr_date_1']['expose']['identifier'] = 'date_until';
  $handler->display->display_options['filters']['field_cr_date_1']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_cr_date_1']['widget_type'] = 'date_popup';
  $handler->display->display_options['filters']['field_cr_date_1']['date_popup_format'] = 'Y-m-d';
  $handler->display->display_options['filters']['field_cr_date_1']['year_range'] = '-10:+0';

  /* Display: Search Service */
  $handler = $view->new_display('services', 'Search Service', 'services_1');
  $handler->display->display_options['path'] = 'search';

  /* Display: Search Page */
  $handler = $view->new_display('page', 'Search Page', 'page_1');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Indexed Node: Node ID */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['link_to_entity'] = 0;
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 0;
  /* Field: Indexed Node: Body */
  $handler->display->display_options['fields']['field_cr_body']['id'] = 'field_cr_body';
  $handler->display->display_options['fields']['field_cr_body']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['field_cr_body']['field'] = 'field_cr_body';
  $handler->display->display_options['fields']['field_cr_body']['label'] = '';
  $handler->display->display_options['fields']['field_cr_body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cr_body']['settings'] = array(
    'data_element_key' => '',
    'skip_safe' => 0,
    'skip_empty_values' => 0,
    'skip_text_format' => 1,
  );
  /* Field: Indexed Node: Meeting Date */
  $handler->display->display_options['fields']['field_cr_date']['id'] = 'field_cr_date';
  $handler->display->display_options['fields']['field_cr_date']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['field_cr_date']['field'] = 'field_cr_date';
  $handler->display->display_options['fields']['field_cr_date']['label'] = 'Date';
  $handler->display->display_options['fields']['field_cr_date']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_cr_date']['alter']['text'] = '[field_cr_date-value]';
  $handler->display->display_options['fields']['field_cr_date']['element_type'] = '0';
  $handler->display->display_options['fields']['field_cr_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => '',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Indexed Node: Topics */
  $handler->display->display_options['fields']['field_cr_topics']['id'] = 'field_cr_topics';
  $handler->display->display_options['fields']['field_cr_topics']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['field_cr_topics']['field'] = 'field_cr_topics';
  $handler->display->display_options['fields']['field_cr_topics']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_cr_topics']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_cr_topics']['bypass_access'] = 0;
  /* Field: Indexed Node: Organizations */
  $handler->display->display_options['fields']['field_cr_organizations']['id'] = 'field_cr_organizations';
  $handler->display->display_options['fields']['field_cr_organizations']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['field_cr_organizations']['field'] = 'field_cr_organizations';
  $handler->display->display_options['fields']['field_cr_organizations']['label'] = 'Organizations:';
  $handler->display->display_options['fields']['field_cr_organizations']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_cr_organizations']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_cr_organizations']['bypass_access'] = 0;
  /* Field: Indexed Node: Attendees */
  $handler->display->display_options['fields']['field_cr_attendees']['id'] = 'field_cr_attendees';
  $handler->display->display_options['fields']['field_cr_attendees']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['field_cr_attendees']['field'] = 'field_cr_attendees';
  $handler->display->display_options['fields']['field_cr_attendees']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_cr_attendees']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_cr_attendees']['bypass_access'] = 0;
  /* Field: Indexed Node: Internal Attendees */
  $handler->display->display_options['fields']['field_cr_internal_attendees']['id'] = 'field_cr_internal_attendees';
  $handler->display->display_options['fields']['field_cr_internal_attendees']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['field_cr_internal_attendees']['field'] = 'field_cr_internal_attendees';
  $handler->display->display_options['fields']['field_cr_internal_attendees']['label'] = 'Internal attendees';
  $handler->display->display_options['fields']['field_cr_internal_attendees']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_cr_internal_attendees']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_cr_internal_attendees']['bypass_access'] = 0;
  /* Field: Indexed Node: Author */
  $handler->display->display_options['fields']['author']['id'] = 'author';
  $handler->display->display_options['fields']['author']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['author']['field'] = 'author';
  $handler->display->display_options['fields']['author']['label'] = 'Author:';
  $handler->display->display_options['fields']['author']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['author']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['author']['view_mode'] = 'full';
  $handler->display->display_options['fields']['author']['bypass_access'] = 0;
  /* Field: Indexed Node: Pin this note */
  $handler->display->display_options['fields']['field_cr_pinned']['id'] = 'field_cr_pinned';
  $handler->display->display_options['fields']['field_cr_pinned']['table'] = 'search_api_index_notes_2';
  $handler->display->display_options['fields']['field_cr_pinned']['field'] = 'field_cr_pinned';
  $handler->display->display_options['fields']['field_cr_pinned']['label'] = '';
  $handler->display->display_options['fields']['field_cr_pinned']['alter']['text'] = '[field_cr_pinned-value]';
  $handler->display->display_options['fields']['field_cr_pinned']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_cr_pinned']['settings'] = array(
    'data_element_key' => '',
    'skip_safe' => 0,
    'skip_empty_values' => 0,
  );
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = '';
  $handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['edit_node']['text'] = 'edit';
  $handler->display->display_options['path'] = 'cricket/search';
  $export['cricket_search'] = $view;

  return $export;
}
