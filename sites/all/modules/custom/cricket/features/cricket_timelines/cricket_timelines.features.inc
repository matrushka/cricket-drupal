<?php
/**
 * @file
 * cricket_timelines.features.inc
 */

/**
 * Implements hook_views_api().
 */
function cricket_timelines_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
