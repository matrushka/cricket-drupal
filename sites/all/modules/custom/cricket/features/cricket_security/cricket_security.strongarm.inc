<?php
/**
 * @file
 * cricket_security.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function cricket_security_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'disable_term_node_listings_cricket_attendees';
  $strongarm->value = '403';
  $export['disable_term_node_listings_cricket_attendees'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'disable_term_node_listings_cricket_organizations';
  $strongarm->value = '403';
  $export['disable_term_node_listings_cricket_organizations'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'disable_term_node_listings_cricket_topics';
  $strongarm->value = '403';
  $export['disable_term_node_listings_cricket_topics'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'disable_term_node_listings_cricket_user_groups';
  $strongarm->value = '403';
  $export['disable_term_node_listings_cricket_user_groups'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'disable_term_node_listings_tags';
  $strongarm->value = '403';
  $export['disable_term_node_listings_tags'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_page_disable_path_node_keep';
  $strongarm->value = 0;
  $export['node_page_disable_path_node_keep'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_frontpage';
  $strongarm->value = 'cricket-app';
  $export['site_frontpage'] = $strongarm;

  return $export;
}
