<?php
/**
 * @file
 * cricket_security.features.roles_permissions.inc
 */

/**
 * Implements hook_default_roles_permissions().
 */
function cricket_security_default_roles_permissions() {
  $roles = array();

  // Exported role: cricket manager
  $roles['cricket manager'] = array(
    'name' => 'cricket manager',
    'weight' => 4,
    'permissions' => array(
      'administer cricket_user_groups vocabulary terms' => TRUE,
      'manage cricket' => TRUE,
    ),
  );

  // Exported role: cricket user
  $roles['cricket user'] = array(
    'name' => 'cricket user',
    'weight' => 3,
    'permissions' => array(
      'administer cricket_attendees vocabulary terms' => TRUE,
      'administer cricket_organizations vocabulary terms' => TRUE,
      'administer cricket_topics vocabulary terms' => TRUE,
      'use cricket' => TRUE,
    ),
  );

  return $roles;
}
