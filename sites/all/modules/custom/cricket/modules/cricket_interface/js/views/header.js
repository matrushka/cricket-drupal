define([
  'jquery',
  'backbone',
  'views/offline-enabled',
  'views/auth',
],
function($, Backbone, OfflineEnabledView, AuthView) {
  var HeaderView = OfflineEnabledView.extend({
    events: {
      'click .cricket-action': 'doCricketAction',
    },

    initialize: function(options) {
      this.childViews = [];
      
      if(_.isObject(options.authModel)) {
        this.authModel = options.authModel;
        this.authModel.on('change:userStatus', this.toggleManagementMenu, this);
      }
      else {
        console.error("HeaderView didn't get a valid Auth model");
      }

      var $authEl = this.$el.find('#cricket-auth-container');
      
      if ($authEl.length == 1) {
        this.authView = new AuthView({model: this.authModel, el: $authEl});
      }

      this.render();
    },

    render: function() {
      this.$el.addClass('cricket-header');
    },

    // This is obsolete because the Cricket interface isn't integrated as a SPA anymore
    // (only the note form will be controlled by Backbone instead), but I will leave it
    // here in case it's useful in the future. It is safe to remove it once we reach V1
    // of the code, and it can be recovered from the backbone-heavy-dev branch.
    doCricketAction: function(e) {
      var $triggerElement = $(e.currentTarget);
      var triggerData = $triggerElement.data('cricket_action');
      var isFullAppAction = triggerData.requiresFullApp;

      // If this is the header for the full app and the action requires the app, then handle the action
      // through the Backbone router. Otherwise, just do the default let the browser navigate to the url
      
      if(!this.fullApp && isFullAppAction) {
        console.log('not doing a thing');
        return; // If not in the full app, just return. The browser will do the default action which is to follow the link.
      }
      else {
        e.preventDefault();
      }


      // The trigger data must be an object, validate before sending the event
      if(!_.isObject(triggerData)) {
        console.error('There was a problem parsing the following Cricket Action options. Please make sure that the data is a valid JSON object.');
        console.debug(triggerData);
        return false;
      }
      else {
        this.trigger('header:cricketAction', triggerData);
      }
    },

    toggleManagementMenu: function() {
      var isManager = false;
      var roles = this.authModel.get('roles');

      for(var rid in roles) {
        var role = roles[rid];
        if(role == 'administrator' || role == 'cricket manager') {
          isManager = true;
          break;
        }
      }

      if(isManager) {
        this.$el.find('.cricket-management').removeClass('hidden');
      }
      else {
        this.$el.find('.cricket-management').addClass('hidden');
      }

    },

  });

  return HeaderView;
});
