require.deps = ['cricket'];
require.urlArgs = 'bust=v0.1';
require.paths = {
  jquery: '../bower_components/jquery/dist/jquery.min',
  backbone: '../bower_components/backbone/backbone',
  underscore: '../bower_components/underscore/underscore-min',
  text: '../bower_components/text/text',
}
require.callback = function(Cricket) {
  // Initialize the Cricket header element
  // This function will check if the header and app-container elements exist
  // and initialize all the required components
  Cricket.init();
}
