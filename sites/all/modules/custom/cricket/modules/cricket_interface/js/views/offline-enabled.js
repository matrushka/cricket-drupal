define([
  'backbone',
],
function(Backbone) {
  var OfflineEnabledView = Backbone.View.extend({
    initialize: function() {
    },
    goOnline: function() {
      console.log('going online');
      console.log(this);
      console.log(this.$el);
      this.$el.addClass('is-online');
      this.$el.removeClass('is-offline');

      if(_.isUndefined(this.childViews)) return;

      for(var i = 0; i < this.childViews.length; i++) {
        var f = this.childViews[i].goOnline;
        if(_.isFunction(f)) {
          this.childViews[i].goOnline();
        }
      }
    },
    goOffline: function() {
      this.$el.addClass('is-offline');
      this.$el.removeClass('is-online');

      if(_.isUndefined(this.childViews)) return;

      for(var i = 0; i < this.childViews.length; i++) {
        var f = this.childViews[i].goOffline;
        if(_.isFunction(f)) {
          this.childViews[i].goOffline();
        }
      }
    },
  });

  return OfflineEnabledView;
});
