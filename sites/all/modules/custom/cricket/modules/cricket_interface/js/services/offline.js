define([
  'jquery'
],
function($) {
  var Offline = {
    baseUrl: '/cricket-is-offline/',

    // Defaults
    // --------------------------------------------------------/
    callback: function() {}, // The callback must be set through the setCallback() function
    //isOnline: true,
    intervalId: null,
    intervalLength: 1000, // interval length set in milliseconds
    
    // Control functions
    // --------------------------------------------------------/
    start: function() {
      var o = this;
      this.stop();
      this.intervalId = setInterval(function() {
        o.test();
      }, this.intervalLength);

      this.test();
    },
    stop: function() {
      if(this.intervalId) {
        clearInterval(intervalId);
        this.intervalId = false;
      }
    },
    setIntervalLength: function(milliseconds) {
      this.intervalLength = milliseconds;
    },
    setTimeout: function(milliseconds) {
      this.timeout = milliseconds;
    },
    setCallback: function(fn) {
      this.callback = fn;
    },
    test: function() {
      var o = this;
      var url = this.baseUrl + Math.round(Math.random() * 10000);

      $.ajax({
        url: url,
        method: 'GET',
        timeout: o.timeout,
        success: function(data) {
          o.setStatus(true);
          
        },
        error: function() {
          o.setStatus(false);
        }
      });
    },
    setStatus: function(isOnline) {
      if(this.isOnline == isOnline) return; // If the status hasn't change don't go around sending events

      this.isOnline = isOnline;
      this.callback(isOnline);
    },
  };

  return Offline;
});
