define([
  'backbone',
],
function(Backbone) {
  var DraftsService = _.extend({
    initialize: function() {
    },
    getDrafts: function() {
      var stored = localStorage.getItem('CricketDrafts');
      if(_.isNull(stored)) {
        return {};
      }
      else {
        return JSON.parse(stored);
      }
    },
    
    // Returns the drafts that are still available for restoring,
    // not including the deleted and already synced drafts
    getRestorableDrafts: function() {
      var d = this.getDrafts();
      
      var restorable = {};
      for(var hash in d) {
        var note = d[hash];
        if(_.isUndefined(note.deleted) && _.isUndefined(note.synced)) {
          restorable[hash] = note;
        }
      }

      return restorable;
    },

    // @param d Object: an object with hash-noteContents pairs
    saveDrafts: function(d) {
      if(!_.isObject(d) || _.isArray(d) || _.isFunction(d)) {
        console.error('Drafts must be an object. Different type received in saveDrafts');
      }

      localStorage.setItem('CricketDrafts', JSON.stringify(d));
    },
    getDraftByHash: function(hash) {
      var d = this.getDrafts();
      
      return d[hash];
    },
    save: function(noteContents) {
      var d = this.getDrafts();

      // If the note already has a hash, avoid saving a new note
      // by using the provided hash 
      if(!_.isEmpty(noteContents.hash) && !_.isUndefined(noteContents.hash)) {
        var hash = noteContents.hash;
      }
      else {
        var hash = this.hashNote(noteContents);
      }
      
      // An empty note will produce a "0" hash. Avoid saving empty notes.
      if(hash === 0) {
        return false;
      }

      d[hash] = noteContents;
      this.saveDrafts(d);

      return true;
    },
    markSynced: function(hash) {
      console.log('TODO: marking drafts as synced causes problem when the client goes offline mid-saving or when the form validation fails');
      // TODO: redesign and implement the workflow for marking as synced.
      return;

      var d = this.getDrafts();
      d[hash].synced = true;

      this.saveDrafts(d);
    },
    markDeleted: function(hash) {
      console.log('marking draft as deleted:');
      console.log(hash);
      var d = this.getDrafts();
      d[hash].deleted = true;

      this.saveDrafts(d);
    },
    hashNote: function(noteContents) {
      // Based on accepted answer from http://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript-jquery#7616484
      var hashString = noteContents.title + noteContents.body;
      var hash = 0, i, chr, len;
      if (hashString.length === 0) return hash;
      for (i = 0, len = hashString.length; i < len; i++) {
        chr   = hashString.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
      }
      return 'n.' + hash;
    },
    prepareNote: function(noteContents) {
      var timestamp = Math.round(Date.now() / 1000);
      noteContents.timestamp = timestamp;
      noteContents.apiVersion = 1;  // While this is not used at the moment, we are going to mark
                                    // the notes as belonging to "version 1". If the fields or their formats
                                    // change in the future, we will be able to identify different versions of the
                                    // notes to convert them to the most recent schema

      return noteContents;
    },
  }, Backbone.Events);

  DraftsService.initialize();

  return DraftsService;
});
