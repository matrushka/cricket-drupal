define([
  'jquery',
  'backbone',
  'views/offline-enabled',
  'views/note-form',
],
function($, Backbone, OfflineEnabledView, NoteFormView) {
  var AppView = OfflineEnabledView.extend({
    initialize: function() {
      this.childViews = [];
      
      var $noteFormEl = this.$el.find('#cricket-note-add-form');
      this.noteFormView = new NoteFormView({el: $noteFormEl});
      this.childViews.push(this.noteFormView);
    },
    render: function() {
    },
    switchView: function(viewName) {
      this.hideAll();
      this[viewName + 'View'].show();
    },
    hideAll: function() {
      for(var i = 0; i < this.childViews.length; i++) {
        this.childViews[i].hide();
      }
    },
  });

  return AppView;
});
