define([
  'jquery',
  'backbone',
  'services/drafts',
  'text!templates/note-form-offline-notice.html',
  'text!templates/save-draft-button.html',
  'text!templates/drafts-list.html',
],
function($, Backbone, Drafts, offline_notice_t, save_draft_button_t, drafts_list_t) {
  var NoteFormView = Backbone.View.extend({
    initialize: function() {
      this.drafts = Drafts;
      this.$leftSidebar = this.$el.find('.left-sidebar');
      this.$rightSidebar = this.$el.find('.right-sidebar');
      this.$form = this.$el.find('#cr-note-node-form');

      this.templates = {
        draftsList: _.template(drafts_list_t)
      };

      this.$fields = {
        title: this.$el.find('#edit-title'),
        body: CKEDITOR.instances['edit-field-cr-body-und-0-value'],
        followUp: this.$el.find('#edit-field-cr-follow-up-und-0-value'),
        meetingDate: this.$el.find('#edit-field-cr-date-und-0-value-datepicker-popup-0'), 
        meetingTime: this.$el.find('#edit-field-cr-date-und-0-value-timeEntry-popup-1'),
        pinned: this.$el.find('#edit-field-cr-pinned-und'),
      };

      this.render();
    },
    events: {
      'click #save-draft': 'saveDraft',
      'click #edit-submit': 'saveNote',
      'click .drafts-list .draft-title': 'toggleDraftActions',
      'click .draft-actions button.restore': 'restoreDraft',
      'click .draft-actions button.delete': 'deleteDraft',
    },
    offlineDisabledInputs: [
      '#edit-field-cr-topics-und',
      '#edit-field-cr-organizations-und',
      '#edit-field-cr-attendees-und',
      '#edit-field-cr-internal-attendees-und',
      '.group-cr-sharing',
      '#edit-submit',
    ],
    offlineHiddenElements: [
      '#edit-field-cr-topics',
      '#edit-field-cr-organizations',
      '#edit-field-cr-attendees',
      '#edit-field-cr-internal-attendees',
      '.group-cr-sharing',
      '#edit-submit',
    ],
    render: function() {
      // Add the drafts list container
      this.$leftSidebar.append('<div id="drafts-list"></div>');
      this.$draftsList = this.$leftSidebar.find('#drafts-list'); 

      // Add the Offline message to the sidebar
      this.$leftSidebar.append(offline_notice_t);

      // Add the Save Draft button
      var $actions = this.$el.find('#edit-actions');
      $actions.append(save_draft_button_t);

      // Add the online-only class to selected elements to hide them while offline
      var $offlineHide = this.$el.find(this.offlineHiddenElements.join(','));
      $offlineHide.addClass('online-only');

      this.renderDraftsList();
    },
    show: function() {
      this.$el.slideDown();
    },
    hide: function() {
      this.$el.hide();
    },
    goOnline: function() {
      var $disable = this.$el.find(this.offlineDisabledInputs.join(','));
      $disable.removeAttr('disabled');
    },
    goOffline: function() {
      this.$el.find(this.offlineDisabledInputs.join(',')).attr('disabled', 'disabled');
    },
    saveDraft: function(e) {
      e.preventDefault();

      // Get an object with the note contents
      var noteContents = this.getNoteContents();
      
      // Save the draft locally
      this.drafts.save(noteContents);
      
      // Notify the user that the note draft was saved, show it in the sidebar
      // and clear the note form contents
      this.renderDraftsList();
      this.clearForm();
    },
    restoreDraft: function(e) {
      // Get the draft by hash
      var $btn = $(e.currentTarget);
      var hash = $btn.data('note_hash');
      var draft = this.drafts.getDraftByHash(hash);

      // Restore the note values to the form
      this.$fields.title.val(draft.title);
      this.$fields.body.setData(draft.body);
      this.$fields.followUp.val(draft.followUp);
      this.$fields.meetingDate.val(draft.meetingDate);
      this.$fields.meetingTime.val(draft.meetingTime);
      this.$fields.pinned.prop('checked', draft.pinned);

      // Add a property to the form indicating the hash note that was restored.
      this.$form.data('restored_note_hash', hash);
    },
    deleteDraft: function(e) {
      var $btn = $(e.currentTarget);
      var hash = $btn.data('note_hash');

      this.drafts.markDeleted(hash); 

      this.renderDraftsList();
    },
    saveNote: function(e) {
      var v = this;
      e.preventDefault();

      var noteContents = this.getNoteContents();
      this.drafts.markSynced(noteContents.hash);

      var tokenError = function() {
        v.$form.prepend('<div class="well well-sm"><p class="text-warning">'+Drupal.t("There was a problem while getting a token from the server. This might be a connection issue. Please save a draft and then try again.")+'</p></div>');
        window.scrollTo(0, 0);
      }

      $.ajax({
        url: '/cricket-form-token',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        success: function(data) {
          if(typeof(data.token) == 'undefined' || data.token.length != 43) {
            tokenError();
            return;
          }

          v.$form.append('<input type="hidden" name="form_token" value="'+data.token+'">');
          v.$form.submit();
        },
        error: function(xhr, textStatus, error) {
          tokenError();
        }
      });
    },
    getNoteContents: function() {
      var contents = {
        hash: this.$form.data('restored_note_hash'),
        title: this.$fields.title.val(),
        body: this.$fields.body.getData(),
        followUp: this.$fields.followUp.val(),
        meetingDate: this.$fields.meetingDate.val(),
        meetingTime: this.$fields.meetingTime.val(),
        pinned: this.$fields.pinned.is(':checked'),
      };

      return contents;
    },
    
    // Reads the drafts list and builds or rebuilds the drafts list
    renderDraftsList: function() {
      var drafts = this.drafts.getRestorableDrafts();
      var data = {};
      data.isEmpty = _.isEmpty(drafts);
      data.drafts = drafts;

      this.$draftsList.empty()
        .append(this.templates.draftsList(data));
    },
    toggleDraftActions: function(e) {
      $(e.currentTarget).parent().toggleClass('show-actions');
    },
    clearForm: function() {
      // Remove the restored hash ID if it exists
      this.$form.data('restored_note_hash', '');

      // Clear all the fields
      this.$fields.title.val('');
      this.$fields.followUp.val('');
      this.$fields.body.setData('');
      this.$fields.pinned.prop('checked', false);
      
      // Set the current date and time
      var now = new Date();
      var currentDate = [(now.getMonth()+1), now.getDate(), now.getFullYear()].join('/');
      var currentTime = [now.getHours(), now.getMinutes()].join(':');
      this.$fields.meetingDate.val(currentDate);
      this.$fields.meetingTime.val(currentTime);
    },
  });

  return NoteFormView;
});
