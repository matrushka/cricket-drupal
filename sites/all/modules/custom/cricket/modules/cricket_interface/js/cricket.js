define([
  'services/offline',
  'jquery',
  'models/auth',
  'views/app',
  'views/header',
  'router',
], 
function(Offline, $, Auth, AppView, HeaderView, CricketRouter) {
  var CricketApp = {
    init: function() {
      this.models = this.models || {};
      this.views = this.views || {};

       _.templateSettings.interpolate  = /\{\{(.*?)\}\}/g;
       _.templateSettings.escape       = /\{\{-([\s\S]+?)\}\}/g;

      // Detect the Cricket App container to decide if we
      // should initialize the full app here, or just the header
      this.$appContainer = $('#cricket-app');
      this.fullApp = this.$appContainer.length == 1;
       
      // Start the Auth model that will handle logging in and out
      // and providing user details to other components
      this.models.auth = new Auth;

      this.initializeHeader();
      this.initializeApp();

      // Start the Offline detection service
      this.initOffline();
    },

    initializeHeader: function() {
      // Instantiate the HeaderView
      var $headerEl = $('#navbar');
      this.views.header = new HeaderView({el: $headerEl, authModel: this.models.auth});
      this.views.header.fullApp = this.fullApp;
      this.views.header.authModel = this.models.auth;

      this.views.header.on('header:cricketAction', this.handleAction, this);
    },

    // Events from the views that aren't handled within the same view
    // are sent to this function so that the app can route them
    handleAction: function(options) {
      if(_.isUndefined(options.action)) {
        console.error("Options don't contain an action. What is the Router supposed to do?");
        return false;
      }

      switch(options.action) {
        case 'showPage':
          this.router.navigate(options.page, {trigger: true});
          break;
        case 'showTimeline':
          this.router.navigate('timelines/' + options.timelineId, {trigger: true});
          break;
      }
    },

    initializeApp: function() {
      // Test if we are in the Cricket app page
      if(this.$appContainer.length != 1) {
        return;
      }

      this.views.app = new AppView({el: this.$appContainer});

      this.initRouter();
    },

    initOffline: function() {
      var app = this;
      Offline.setIntervalLength(5000);
      Offline.setTimeout(1000);
      Offline.setCallback(function(isOnline) {
        if(isOnline) {
          if(!_.isUndefined(app.views.app)) {
            app.views.app.goOnline();
          }

          app.views.header.goOnline();
        }
        else {
          if(!_.isUndefined(app.views.app)) {
            app.views.app.goOffline();
          }

          app.views.header.goOffline();
        }
      });
      Offline.start();
    },
    initRouter: function() {
      this.router = CricketRouter;
      this.router.app = this;
      Backbone.history.start({pushState: true, root: '/cricket-home/'});
    },
  }

  return CricketApp;
});
