define([
  'jquery',
  'backbone',
  'underscore',

  'text!templates/user-info.html',
  'text!templates/user-login.html',
],

function($, Backbone, _, user_info_t, user_login_t) {
  var AuthView = Backbone.View.extend({
    tagName: 'div',
    className: 'cricket-auth-view',
    el: $('#cricket-auth-container'),

    events: {
      'click .toggle-form': 'toggleForm',
      'click #cricket-login-submit': 'doLogin', 
      'click #cricket-logout': 'doLogout',
      'keyup .cricket-login': 'loginEnter',
    },

    initialize: function() {

      this.templates = {
        user_info: _.template(user_info_t), 
        user_login: _.template(user_login_t), 
      };

      this.model.on('change:userStatus', this.userChange, this);
    },

    render: function() {
      this.$el.empty().append(this.templates.user_info({testvar: 'esta no te la sabías'}));
    },

    doLogin: function() {
      var v = this;
      var username = this.$el.find('#cricket-login-username').val();
      var password = this.$el.find('#cricket-login-password').val();

      this.clearLoginError();

      this.model.logIn(username, password, function(err, data) {
        if(err) {
          v.showLoginError(err);
        }
      });
    },

    showLoginError: function(error) {
      this.$el.find('#cricket-login-error').empty().text(error).removeClass('hidden');
    },

    clearLoginError: function() {
      this.$el.find('#cricket-login-error').empty().text('').addClass('hidden');;
    },

    doLogout: function() {
      this.model.logOut(function() {
        location.reload();
      });
    },

    userChange: function() {
      var status = this.model.get('userStatus');

      if(status == 'logged in') {
        console.log(this.model.toJSON());
        this.$el.empty().append(this.templates.user_info(this.model.toJSON()));
      }
      else {
        this.$el.empty().append(this.templates.user_login());
      }
    },

    toggleForm: function() {
      var $login = this.$el.find('.cricket-login');
      var $toggle = $login.find('.toggle-form');
      var $icon = $toggle.find('.glyphicon');

      $login.toggleClass('open');
      if($login.hasClass('open')) {
        $icon.removeClass('glyphicon-plus').addClass('glyphicon-minus');
      }
      else {
        $icon.removeClass('glyphicon-minus').addClass('glyphicon-plus');
      }
    },

    loginEnter: function(evt) {
      if(evt.keyCode == 13) {
        this.doLogin();
      }
    },

    showLoading: function() {
    },

    showAnonymous: function() {
    },

    showUserInfo: function() {
    }
  });

  return AuthView;
});
