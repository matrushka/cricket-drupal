define([
  'jquery',
  'backbone'
],

function($, Backbone) {
  var Auth = Backbone.Model.extend({
    initialize: function() {
      var a = this;

      this.url = '/cricket-api/v1';
      this.getCurrentUser(function(err, data) {
        if(err) {
          a.clearUser();
        }
        else {
          a.setUserData(data.user);
        }
      });
    },

    getCurrentUser: function(callback) {
      var a = this;
      this.fetchToken(function(err, data) {
        if(err) {
          if(callback) callback(err);
          return;
        }
        else {
          $.ajax({
            url: a.url + '/system/connect.json',
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'X-CSRF-Token': data.token,
            },
            success: function(data) {
              if(callback) callback(false, data);
            },
            error: function(xhr, textStatus, error) {
              if(callback) callback(error);
              a.clearUser();
            }
          });
        }
      });
      
    },

    logIn: function(username, password, callback) {
      var a = this;
      this.fetchToken(function(err, data) {
        if(err) {
          if(callback) callback(err);
          return;
        }
        else {
          $.ajax({
            url: a.url + '/user/login.json',
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'X-CSRF-Token': data.token,
            },
            data: JSON.stringify({
              username: username,
              password: password
            }),
            contentType: 'application/json; charset=UTF-8',
            success: function(data) {
              a.setUserData(data.user);
              if(callback) callback(false, data);
            },
            error: function(xhr, textStatus, error) {
              if(callback) callback(error);
              a.clearUser();
            }

          });
        }
      });
    },

    logOut: function(callback) {
      var a = this;
      this.fetchToken(function(err, data) {
        if(err) {
          if(callback) callback(err);
          return;
        }
        else {
          $.ajax({
            url: a.url + '/user/logout.json',
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'X-CSRF-Token': data.token,
            },
            contentType: 'application/json; charset=UTF-8',
            success: function(data) {
              a.clearUser();
              if(callback) callback(false, data);
            },
            error: function(xhr, textStatus, error) {
              if(callback) callback(error);
            }
          });
        }
      });

    },

    fetchToken: function(callback) {
      var a = this;

      $.ajax({
        url: this.url + '/user/token.json',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        success: function(data) {
          if(callback) callback(false, data);
        },
        error: function(xhr, textStatus, error) {
          if(callback) callback('error');
          a.clearUser();
        }
      });
    },

    // Saves user data for use in this session
    setUserData: function(user) {
      if(user.uid == 0) {
        this.clearUser();
        return;
      }

      this.set('uid', user.uid);
      this.set('mail', user.mail);
      this.set('roles', user.roles);
      this.set('name', user.name);

      this.set('userStatus', 'logged in');
    },

    clearUser: function() {
      this.set('uid', null);
      this.set('mail', null);
      this.set('roles', null);
      this.set('name', null);

      this.set('userStatus', 'logged out');
    },


  });

  return Auth;
});
