define([
  'backbone'
],
function(Backbone) {
  // return CricketRouter;
  var CricketRouter = Backbone.Router.extend({
    routes: {
      '': 'noteForm',
      'note-form': 'noteForm',
      'timelines/:timelineId': 'showTimeline',
    },

    noteForm: function() {
      this.app.views.app.switchView('noteForm');
    },

    showTimeline: function(timelineId) {
      this.app.views.app.showTimeline(timelineId);
    },

  });

  var r = new CricketRouter;

  return r;
});
