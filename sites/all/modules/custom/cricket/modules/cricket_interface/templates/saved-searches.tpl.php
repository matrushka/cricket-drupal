<div class="saved-searches">
  <h4>Saved timelines</h4>
  <?php if($saved): ?>
    <div class="saved-list">
      <ul>
      <?php foreach($saved as $s): 
        $criteria = json_decode($s['criteria'], true);
        $link = l($s['name'], 'cricket/search', array('query' => $criteria, 'attributes' => array('class' => array('saved-search-name'))));
        $delete_form = drupal_get_form('cricket_saved_searches_delete_' . $s['id'], $s['id']);
        ?>
        <li class="saved-search-item clearfix"><?php print $link; print drupal_render($delete_form); ?> </li>
      <?php endforeach; ?>
      </ul>
    </div>
  <?php endif; ?>
  <div class="save-form">
    <h5>Save the current search</h5>
    <?php print drupal_render($form); ?>
  </div>
</div>
