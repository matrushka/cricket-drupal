<div id="cricket-app">
  <div class="cricket-view" id="cricket-note-add-form">
  <!--
  Developer note: The markup for this section is served from Drupal and later altered in the client side app. See cricket-home.tpl.php
  -->
    <div class="row">
      <div class="col-md-3 left-sidebar"></div>
      <div class="col-md-6 form-container"><?php print $note_form; ?></div>
      <div class="col-md-3 right-sidebar"></div>
    </div>
    
  </div>
</div>
