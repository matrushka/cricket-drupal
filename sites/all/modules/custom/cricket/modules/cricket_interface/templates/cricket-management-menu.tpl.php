<?php global $base_path; ?>
<li class="dropdown cricket-management hidden">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php print t('Management'); ?><span class="caret"></span></a>
  <ul class="dropdown-menu">
    <li><a href="<?php print $base_path; ?>admin/people?destination=cricket-app"><?php print t('Manage users'); ?></a></li>
    <li><a href="<?php print $base_path; ?>admin/structure/taxonomy/cricket_user_groups?destination=cricket-app"><?php print t('Manage groups'); ?></a></li>
  </ul>
</li>
