<?php
global $base_path;
?>

<ul class="nav navbar-nav">
  <li><a href="<?php print $base_path; ?>cricket-app">New note</a></li>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle online-only" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php print t('Quick read'); ?><span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="<?php print $base_path; ?>cricket/latest-notes"><?php print t('Latest'); ?></a></li>
      <li><a href="<?php print $base_path; ?>cricket/my-notes"><?php print t('My notes'); ?></a></li>
      <li><a href="<?php print $base_path; ?>cricket/pinned-notes"><?php print t('Pinned'); ?></a></li>
    </ul>
  </li>

  <?php 
  // The cricket management menu is rendered separately to
  // do access control in the module instead of here.
  print $menu; ?>
</ul>

<form class="navbar-form navbar-left online-only" action="<?php print $base_path; ?>cricket/search" method="GET">
  <div class="form-group">
    <input type="text" class="form-control" placeholder="search terms" name="fulltext">
  </div>
  <button type="submit" class="btn btn-default btn-small">Search</button>
</form>

<div class="navbar-right">
  <div id="cricket-auth-container"><span class="glyphicon glyphicon-time"></span> loading</div>
</div>
<!--
<div id="cricket-header">
  <div class="menu-container"></div>
</div>
-->
