# Cricket Docker

The configuration contained here simplifies the instantiation of a Solr server. Instead of installing and configuring one yourself, just start the provided Docker container. Hasn't been tested for production use.

## Getting started

### Requirements

—Docker installed

### Launch

The following steps assume that you are using the _default_ machine, and that you are working under Mac or Linux. Open your terminal and then:

* Check that the Docker machine is running with `docker-machine ls`. You should get a row with _default_ in the _name_ column.
* If it's not running, then start it with `docker-machine start default`.
* To set the environment variables needed for _docker-compose_ to work, run `eval $(docker-machine env)`.
* In the directory that contains this README, run `docker-compose up -d`. If the images were built before, they will just be started. When you run this command for the first time, the Docker image will be built. This might take a few minutes.
