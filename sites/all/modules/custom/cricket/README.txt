***********************************************
Cricket
***********************************************

Cricket is a module that helps teams take meeting notes, share them and search them. They can tag people that attended the meetings, organizations and topics, and it has a fine-grained sharing system that allows the creator of a note to configure precisely who should have access to the note.

While this is currently just a module, it is thought of as a Drupal distribution, and once enabled, the modules take over features like the front page, making the whole Drupal instance devoted to Cricket. It is possible, however, to leave the cricket interface off, and to integrate the notes system in a different way.

## Getting started

Follow these steps
* Install Drupal, if you haven't already.
* Enable the following modules: cricket cricket_security crt cricket_interface cricket_interface_settings cricket_search_configuration adminimal
* Disable some modules (optional): overlay color comment
* Set the Cricket theme as the default.
* Log out
* Visit the front page

Cricket uses appcache to allow the offline use of the note taking form. Logging out before going to the front page prevents your browser from caching the logged-in version that might contain toolbars and other admin tools. This wouldn't be a problem for your users anyway.

That's it, start using Cricket.
