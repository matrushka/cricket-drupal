<?php
/**
 * @file
 * The primary PHP file for this theme.
 */

/**
  * Implementation of hook_theme();
  */
function crt_theme() {
  $path = drupal_get_path('theme', 'crt');

  return array(
    'crt_exposed_form_widget' => array(
      'path' => $path . '/templates/',
      'template' => 'crt-exposed-form-widget',
      'variables' => array(
        'widget' => NULL,
      ),
    ),
  );
}

function crt_preprocess_node(&$variables) {
  global $user;

  $variables['note_content'] = array('main' => array(), 'meta' => array(), 'access' => array());
  
  $content = $variables['content'];
  $main = &$variables['note_content']['main'];
  $meta = &$variables['note_content']['meta'];
  $access = &$variables['note_content']['access'];

  $main_fields = array('field_cr_body', 'field_cr_follow_up');
  $meta_fields = array('field_cr_date', 'field_cr_topics', 'field_cr_organizations', 'field_cr_attendees', 'field_cr_internal_attendees');
  $access_fields = array('field_cr_sharing_everyone', 'field_cr_sharing_users', 'field_cr_sharing_groups');

  foreach($main_fields as $mf) {
    if(isset($content[$mf])) $main[$mf] = $content[$mf];
  }

  foreach($meta_fields as $mef) {
    if(isset($content[$mef])) $meta[$mef] = $content[$mef];
  }

  foreach($access_fields as $af) {
    if(isset($content[$af])) $access[$af] = $content[$af];
  }

  $variables['is_pinned'] = @$variables['field_cr_pinned'][0]['value'] == 1;
  $variables['own_note'] = $variables['uid'] == $user->uid;

  if($variables['own_note']) {
    $variables['classes_array'][] = 'own-note';
  }
  $note_author = user_load($variables['uid']);
  $variables['author'] = $note_author;
}
