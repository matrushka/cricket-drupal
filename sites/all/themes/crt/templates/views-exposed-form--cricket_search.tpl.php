<?php

/**
 * @file
 * This template handles the layout of the views exposed filter form.
 *
 * Variables available:
 * - $widgets: An array of exposed form widgets. Each widget contains:
 * - $widget->label: The visible label to print. May be optional.
 * - $widget->operator: The operator for the widget. May be optional.
 * - $widget->widget: The widget itself.
 * - $sort_by: The select box to sort the view using an exposed form.
 * - $sort_order: The select box with the ASC, DESC options to define order. May be optional.
 * - $items_per_page: The select box with the available items per page. May be optional.
 * - $offset: A textfield to define the offset of the view. May be optional.
 * - $reset_button: A button to reset the exposed filter applied. May be optional.
 * - $button: The submit button for the form.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($q)): ?>
  <?php
    // This ensures that, if clean URLs are off, the 'q' is added first so that
    // it shows up first in the URL.
    print $q;
  ?>
<?php endif; ?>
<script language="javascript">
  (function($) {
    $(function() {
      var $toggle = $('.views-exposed-form .toggle-advanced-search');
      var $advanced = $('.advanced-search');
      
      // When the toggle is clicked, er... toggle the advanced search
      $toggle.click(function() {
        console.log('clicked!');
        $advanced.toggleClass('open');

        if($advanced.hasClass('open')) {
          $toggle.find('.glyphicon').removeClass('glyphicon-plus').addClass('glyphicon-minus');
        }
        else {
          $toggle.find('.glyphicon').removeClass('glyphicon-minus').addClass('glyphicon-plus');
        }
      });
    });
  })(jQuery);
</script>
<div class="views-exposed-form">
  <div class="views-exposed-widgets clearfix row">

    <div class="fulltext col-lg-12">
      <?php print theme('crt_exposed_form_widget', array('widget' => $widgets['filter-search_api_views_fulltext'])); ?>
      <h4 class="toggle-advanced-search">Advanced search &nbsp; <span class="badge"><span class="glyphicon glyphicon-plus"></span></span></h4>
    </div>
    
    <div class="advanced-search">
      <div class="meta-left col-lg-6 col-sm-12">
        <?php print theme('crt_exposed_form_widget', array('widget' => $widgets['filter-field_cr_topics'])); ?>
        <?php print theme('crt_exposed_form_widget', array('widget' => $widgets['filter-field_cr_organizations'])); ?>
        <?php print theme('crt_exposed_form_widget', array('widget' => $widgets['filter-field_cr_attendees'])); ?>
        <?php print theme('crt_exposed_form_widget', array('widget' => $widgets['filter-field_cr_internal_attendees'])); ?>
      </div>

      <div class="meta-right col-lg-6 col-sm-12">
        <?php print theme('crt_exposed_form_widget', array('widget' => $widgets['filter-field_cr_date'])); ?>
        <?php print theme('crt_exposed_form_widget', array('widget' => $widgets['filter-field_cr_date_1'])); ?>

        <?php if (!empty($sort_by)): ?>
          <div class="views-exposed-widget views-widget-sort-by">
            <?php print $sort_by; ?>
          </div>
          <div class="views-exposed-widget views-widget-sort-order">
            <?php print $sort_order; ?>
          </div>
        <?php endif; ?>
        <?php if (!empty($items_per_page)): ?>
          <div class="views-exposed-widget views-widget-per-page">
            <?php print $items_per_page; ?>
          </div>
        <?php endif; ?>
        <?php if (!empty($offset)): ?>
          <div class="views-exposed-widget views-widget-offset">
            <?php print $offset; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>

    <div class="actions col-lg-12">
      <div class="views-exposed-widget views-submit-button">
        <?php print $button; ?>
      </div>
      <?php if (!empty($reset_button)): ?>
        <div class="views-exposed-widget views-reset-button">
          <?php print $reset_button; ?>
        </div>
      <?php endif; ?>
    </div>

  </div>
</div>
